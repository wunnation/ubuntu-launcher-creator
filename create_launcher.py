# 1. Add Categories: Categories=AudioVideo;Player;Recorder;Application;Development;
# 2. Add Keywords: Keywords=Player;Capture;DVD;Audio;Video;Server;Broadcast;
# 3. Execution Options: Exec=/usr/bin/vlc --started-from-file %U
# 4. Tray Execution: TryExec=/usr/bin/vlc
# 5. Version: Version=1.0
# 6. Add Shortcut Group for adding menu entries: [NewWindow Shortcut Group] (google-chrome)
# 7. Only Show in: OnlyShowIn=GNOME;Unity;
# 8. NoDisplay=false (Show in unity HUD Search Results) (checkbox)

from PyQt4 import QtGui, QtCore
import sys
import os
#[Desktop Entry]
#F. NoDisplay=true -> don't show in Unity HUD, NoDisplay=false -> Show in Unity HUD
#G. StartupNotify=true
#H. Icon=/home/letmein/.local/share/applications/independent_monitors_icon.png # Icon Path QLine Edit (w/button)
#I. Save Button(defaults to home folder)

#A. Terminal=true   # Checkbox
class GUI_UseTerminal():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Open Terminal")
        self.checkbox = QtGui.QCheckBox()

#B. Type=Application  # Combobox
class GUI_Type():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Launcher Type")
        self.combobox = QtGui.QComboBox()
        self.combobox.addItem("Application")

#C. Name=MonitorsIndependent  # Line Edit
class GUI_Name():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Name")
        self.lineedit = QtGui.QLineEdit("My Application")

#D. Exec=/bin/sh /home/letmein/.screenlayout/two_computers.sh %f # Line Edit with Button
class GUI_RunCommand():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Run")
        self.lineedit = QtGui.QLineEdit("My Application Path")
        self.button = QtGui.QPushButton("...") # Get Application Path Button

#E. Allow Adding To Dock (adds %f)
class GUI_AllowDocking():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Allow Adding to Dock")
        self.checkbox = QtGui.QCheckBox()

#I. Icon
class GUI_IconLocation():
    def __init__(self, parent=None):
        self.label = QtGui.QLabel("Icon:")
        self.lineedit = QtGui.QLineEdit("My Icon Path")
        self.button = QtGui.QPushButton("...") # Get Application Path Button

class UbuntuDesktopFileCreator(QtGui.QDialog):
    def __init__(self, parent=None):
        super(UbuntuDesktopFileCreator, self).__init__(parent)
        # Declare All Widgets
        self.type = GUI_Type()
        self.name = GUI_Name()
        self.run = GUI_RunCommand()
        self.terminal = GUI_UseTerminal()
        self.docking = GUI_AllowDocking()
        self.icon = GUI_IconLocation()
        self.file_dialog = QtGui.QFileDialog()
        self.save = QtGui.QPushButton("Save")

        # Connect Widgets to Functions
        self.save.clicked.connect(self.CreateDesktopFile)
        self.icon.button.clicked.connect(self.SelectIconFile)
        self.run.button.clicked.connect(self.SelectRunFile)

        # Setup Layout
        self.layout = QtGui.QGridLayout()
        self.layout.addWidget(self.type.label, 0, 0)
        self.layout.addWidget(self.type.combobox, 0, 1)
        self.layout.addWidget(self.name.label, 1, 0)
        self.layout.addWidget(self.name.lineedit, 1, 1)
        self.layout.addWidget(self.run.label, 2, 0)
        self.layout.addWidget(self.run.lineedit, 2, 1)
        self.layout.addWidget(self.run.button, 2, 2)
        self.layout.addWidget(self.terminal.label, 3, 0)
        self.layout.addWidget(self.terminal.checkbox, 3, 1)
        self.layout.addWidget(self.docking.label, 4, 0)
        self.layout.addWidget(self.docking.checkbox, 4, 1)
        self.layout.addWidget(self.icon.label, 5, 0)
        self.layout.addWidget(self.icon.lineedit, 5, 1)
        self.layout.addWidget(self.icon.button, 5, 2)
        self.layout.addWidget(self.save, 6, 2)
        self.setLayout(self.layout)
        self.setMinimumWidth(512)

    def SelectIconFile(self):
        file_path = self.file_dialog.getOpenFileName()
        if len(file_path):
            self.icon.lineedit.setText(file_path)

    def SelectRunFile(self):
        file_path = self.file_dialog.getOpenFileName()
        if len(file_path):
            self.run.lineedit.setText(file_path)

    def CreateDesktopFile(self):
        file_text = self.GenerateDesktopFileText()
        save_file_dir = ''
        save_file_path = self.file_dialog.getSaveFileName(directory=save_file_dir)
        if len(save_file_path):
            try:
                fd = open(save_file_path, 'w')
                fd.write(file_text)
                fd.close()
                chmod_text = 'chmod +x ' + str(save_file_path)
            except:
                chmod_text = ''
                print "failed to write"
        else: # No Save File Selected
            chmod_text = ''

        # Add Executable Permissions
        if len(chmod_text):
            return_text = os.popen(chmod_text)
            return_text = return_text.read()
            if len(return_text):
                print "chmod fail? " + return_text

    def GenerateDesktopFileText(self):
        file_text = '[Desktop Entry]\n'
        file_line = 'Terminal='
        if self.terminal.checkbox.isChecked():
            file_line += 'True'
        else:
            file_line += 'False'
        file_line += '\n'
        file_text += file_line
        file_line = 'Type='
        file_line += self.type.combobox.itemText(self.type.combobox.currentIndex()) + '\n'
        file_text += file_line
        file_line = 'Name='
        file_line += self.name.lineedit.text() + '\n'
        file_text += file_line
        file_line = 'Exec='
        file_line += self.run.lineedit.text()
        if self.docking.checkbox.isChecked():
            file_line += ' %f'
        file_line += '\n'
        file_text += file_line
        file_text += 'NoDisplay=false\n' # Allow to display in Unity HUD Search Results
        file_text += 'StartupNotify=true\n'
        if len(self.icon.lineedit.text()):
            file_line = 'Icon=' 
            file_line += self.icon.lineedit.text() + '\n'
            file_text += file_line
        return file_text

if __name__ == '__main__':
    if sys.flags.interactive:
        import rlcompleter
        import readline
        readline.parse_and_bind("tab: complete")
        app = QtGui.QApplication([])
        win = UbuntuDesktopFileCreator()
        win.show()
    else:
        def main():
            app = QtGui.QApplication([])
            win = UbuntuDesktopFileCreator()
            win.show()
        main()
